const { response } = require('express');
const transactionRepository = require('../../../repositories/transactionRepository');

const getTransactions = async (req, res = response) => {
    try {
        const transactions = await transactionRepository.getAll();
        const count = await transactionRepository.count();
        if (!transactions) {
            return res.status(401).json({
                message: 'Not found',
            })
        }
        res.status(200).json({
            message: 'Transactions',
            response: transactions,
            total: count
        })
    } catch (error) {
        console.log(error)
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

const getTransactionId = async (req, res = response) => {
    try {
        console.log(req.params)
        console.log(req.params.id)
        const id = req.params.id
        const transactionId = await transactionRepository.getOne(id);

        if (!transactionId) {
            return res.status(401).json({
                message: 'Not found',
            })
        }
        return res.status(200).json({
            message: 'TransactionId',
            data: transactionId
        })
    } catch (error) {
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

module.exports = {
    getTransactions,
    getTransactionId
}