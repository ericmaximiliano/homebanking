const getAll = require('./transactionCase/getTransaction/getTransaction.js').getTransactions;
const getOne = require('./transactionCase/getTransaction/getTransaction.js').getTransactionId;
const createTransaction = require('../transaction/transactionCase/createTransaction/createTransaction');

module.exports = {
    getAll,
    getOne,
    createTransaction
}