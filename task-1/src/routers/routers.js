const { Router } = require('express');
const router = new Router();
// clients
const getClients = require('../client/clientController');
const getClientId = require('../client/clientController');
const { createClient } = require('../client/clientController');
router.get('/clients', getClients.getAll);
router.get('/clients/:id', getClientId.getOne);
router.post('/clients', createClient.createClient);
// accounts
const getAccounts = require('../account/accountController');
const getAccountId = require('../account/accountController');
const { createAccount } = require('../account/accountController');
router.get('/accounts', getAccounts.getAll);
router.get('/accounts/:id', getAccountId.getOne);
router.post('/accounts', createAccount.createAccount);
// transaction
const getTransactions = require('../transaction/transactionController');
const getTransactionId = require('../transaction/transactionController');
const { createTransaction } = require('../transaction/transactionController');
router.get('/transactions', getTransactions.getAll);
router.get('/transactions/:id', getTransactionId.getOne);
router.post('/transactions', createTransaction.createTransaction);
// loan
const getLoans = require('../loan/loanController');
const getLoanId = require('../loan/loanController');
const { createLoan } = require('../loan/loanController');
router.get('/loans', getLoans.getAll);
router.get('/loans/:id', getLoanId.getOne);
router.post('/loans', createLoan.createLoan);
// loan application
//const getLoanAplication = require('../loanAplication/loanAplicationController');
//const getLoanAplicationId = require('../loanAplication/loanAplicationController');
const { createLoanAplication } = require('../loanAplication/loanAplicationController');
//router.get('/loanaplications', getLoanAplication.getAll);
//router.get('/loanaplications/:id', getLoanAplicationId.getOne);
router.post('/loanaplications', createLoanAplication.createLoanAplication);
// auth
const userCreate = require('../user/authController');
// exporto todo
module.exports = router;
