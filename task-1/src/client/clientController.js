const getAll = require('./clientCase/getClients/getClients').getClients;
const getOne = require('./clientCase/getClients/getClients').getClientId;
const createClient = require('../client/clientCase/createClient/createClient');

module.exports = {
    getAll,
    getOne,
    createClient
}