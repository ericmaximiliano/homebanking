const { response } = require('express');
const clientRepository = require('../../../repositories/clientRepository');

const getClients = async (req, res = response) => {
    try {
        const clients = await clientRepository.getAll();
        const count = await clientRepository.count();
        if (!clients) {
            return res.status(401).json({
                message: 'Not found',
            })
        }
        res.status(200).json({
            message: 'Clients',
            response: clients,
            total: count
        })
    } catch (error) {
        console.log(error)
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

const getClientId = async (req, res = response) => {
    try {
        console.log(req.params)
        console.log(req.params.id)
        const id = req.params.id
        const clientId = await clientRepository.getOne(id);

        if (!clientId) {
            return res.status(401).json({
                message: 'Not found',
            })
        }
        return res.status(200).json({
            message: 'ClientId',
            data: clientId
        })
    } catch (error) {
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

module.exports = {
    getClients,
    getClientId
}