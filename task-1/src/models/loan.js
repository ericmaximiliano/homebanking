const mongoose = require('mongoose');

const loanSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    maxAmount: {
        type: Number,
        required: true
    },
    payments: [{
        type: Number,
        required: true
    }]
})

loanSchema.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('loan', loanSchema);