const mongoose = require('mongoose');

const loanAppSchema = new mongoose.Schema({
    payments: {
        type: Number,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    client: {
        type: mongoose.Schema.Types.ObjectId, ref: 'client'
    },
    loan: {
        type: mongoose.Schema.Types.ObjectId, ref: 'loan'
    },
})

//loanAppSchema.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('loanApp', loanAppSchema);