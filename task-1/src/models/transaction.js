const mongoose = require('mongoose');

const transactionSchema = new mongoose.Schema({
    type: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        default: 0,
        required: true
    },
    description: {
        type: String,
        default: ""
    },
    date: {
        type: Date,
        default: Date.now,
        required: true
    },
    account: {
        type: mongoose.Schema.Types.ObjectId, ref: 'account'
    }
})

module.exports = mongoose.model('transaction', transactionSchema);