const getAll = require('./loanCase/getLoan/getLoan.js').getLoans;
const getOne = require('./loanCase/getLoan/getLoan.js').getLoanId;
const createLoan = require('../loan/loanCase/createLoan/createLoan');

module.exports = {
    getAll,
    getOne,
    createLoan
}