const { response } = require('express');
const loanRepository = require('../../../repositories/loanRepository');

const getLoans = async (req, res = response) => {
    try {
        const loans = await loanRepository.getAll();
        console.log(loans)
        const count = await loanRepository.count();
        if (!loans) {
            return res.status(401).json({
                message: 'Not found',
            })
        }
        res.status(200).json({
            message: 'Loans',
            response: loans,
            total: count
        })
    } catch (error) {
        console.log(error),
            res.status(500).json({
                message: 'Error Interno del Servidor',
                err: error
            })
    }
}

const getLoanId = async (req, res = response) => {
    try {
        console.log(req.params)
        console.log(req.params.id)
        const id = req.params.id
        const loanId = await loanRepository.getOne(id);

        if (!loanId) {
            return res.status(401).json({
                message: 'Not found',
            })
        }
        return res.status(200).json({
            message: 'LoanId',
            data: loanId
        })
    } catch (error) {
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

module.exports = {
    getLoans,
    getLoanId
}