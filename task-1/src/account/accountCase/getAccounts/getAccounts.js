const { response } = require('express');
const accountRepository = require('../../../repositories/accountRepository');

const getAccounts = async (req, res = response) => {
    try {
        const accounts = await accountRepository.getAll();
        const count = await accountRepository.count();
        if (!accounts) {
            return res.status(401).json({
                message: 'Not found',
            })
        }
        res.status(200).json({
            message: 'Accounts',
            response: accounts,
            total: count
        })
    } catch (error) {
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

const getAccountId = async (req, res = response) => {
    try {
        console.log(req.params)
        console.log(req.params.id)
        const id = req.params.id
        const accountId = await accountRepository.getOne(id);

        if (!accountId) {
            return res.status(401).json({
                message: 'Not found',
            })
        }
        return res.status(200).json({
            message: 'AccountId',
            data: accountId
        })
    } catch (error) {
        res.status(500).json({
            message: 'Error Interno del Servidor',
            err: error
        })
    }
}

module.exports = {
    getAccounts,
    getAccountId
}