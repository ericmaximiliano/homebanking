const { response } = require('express');

const accountRepository = require('../../../repositories/accountRepository');

const createAccount = async (req, res = response) => {
    try {
        console.log(req.body)
        await accountRepository.save(req.body);

        return res.status(201).json({
            message: 'La cuenta se creo correctamente',
        })
    } catch (error) {
        console.log(error)

        return res.status(500).json({
            message: 'Error interno del servidor',
            err: error
        })
    }
}

module.exports = { createAccount };