const getAll = require('./accountCase/getAccounts/getAccounts.js').getAccounts;
const getOne = require('./accountCase/getAccounts/getAccounts.js').getAccountId;
const createAccount = require('../account/accountCase/createAccount/createAccount');

module.exports = {
    getAll,
    getOne,
    createAccount
}