//const { response } = require("express")
//requiere model
const jwt = require("jsonwebtoken")

module.exports.login_post = async (req, res) => {
    const { email, password } = req.body
    const user = await User.findOne({ email: email })
    if (!user) {
        return res.status(400).json({ error: "usuario no encontrado" })
    }
    if (password !== user.password) {
        return res.status(400).json({ error: "contraseña incorrecta" })
    }

    // crear un token
    const token = jwt.sign({
        name: user.name,
        id: user._id
    }, process.env.SECRET_KEY)
    res.status(200).json({
        message: "Bievenido!",
        token: token
    })

}