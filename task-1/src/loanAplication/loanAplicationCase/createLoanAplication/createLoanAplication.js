const { response } = require('express');

const loanAplicationRepository = require('../../../repositories/loanAplicationRepository');

const createLoanAplication = async (req, res = response) => {
    try {
        console.log(req.body)
        await loanAplicationRepository.save(req.body);

        return res.status(201).json({
            message: 'El prestamo se registro correctamente',
        })
    } catch (error) {
        console.log(error)

        return res.status(500).json({
            message: 'Error interno del servidor',
            err: error
        })
    }
}

module.exports = { createLoanAplication };