const LoanAplication = require('../models/loan_application');
const Client = require('../models/client');
const getAll = async () => await LoanAplication.find();
const getOne = async (id) => await LoanAplication.findById(id);
const count = async () => await LoanAplication.count();


const save = async (body) => {
    const loanAplication = new LoanAplication({
        payments: body.payments,
        amount: body.amount,
        client: body.client,
        loan: body.loan
    })
    const loanApp = await loanAplication.save();
    const client = await Client.findById(loanApp.client);
    client.loans.push(loanApp._id);
    await Client.updateOne({ _id: client._id }, {
        loans: client.loans
    });
}

module.exports = {
    getAll,
    getOne,
    count,
    save
}