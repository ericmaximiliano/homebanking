const Account = require('../models/account');
const Client = require('../models/client');
const getAll = async () => await Account.find();
const getOne = async (id) => await Account.findById(id);
const count = async () => await Account.count();


const save = async (body) => {
    const account = new Account({
        number: body.number,
        client: body.client
        // createDate: body.createDate,
        // balance: body.balance,
    })

    const accountSaveId = await account.save();
    console.log(accountSaveId);
    const client = await Client.findById(accountSaveId.client);
    console.log(client);

    client.accounts.push(accountSaveId._id);

    const updateclient = await Client.updateOne({ _id: client._id }, {
        accounts: client.accounts
    });
    console.log(updateclient);
    return account;
}

module.exports = {
    getAll,
    getOne,
    count,
    save
}