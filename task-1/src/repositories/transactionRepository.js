const Account = require('../models/account');
const Transaction = require('../models/transaction');
//const TransactionType = require('../models/transactionType');
const getAll = async () => await Transaction.find();
const getOne = async (id) => await Transaction.findById(id);
const count = async () => await Transaction.count();

const save = async (body) => {
    const transaction = new Transaction({
        // type: TransactionType.DEBIT,
        type: body.type,
        amount: body.amount,
        description: body.description,
        account: body.account
    })

    const transactionSave = await transaction.save();
    console.log(transactionSave);
    const account = await Account.findById(transactionSave.account);
    console.log(account);

    account.transactions.push(transactionSave._id);

    const updateaccount = await Account.updateOne({ _id: account._id }, {
        transactions: account.transactions
    });
    console.log(updateaccount);

    return await transaction.save();
}

module.exports = {
    getAll,
    getOne,
    count,
    save
}