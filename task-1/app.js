require('dotenv').config()
const express = require('express')
const cors = require('cors')
const app = express()
const port = 3000
const mongoose = require('mongoose')
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors())
app.get('/clients', (req, res) => {
  res.send('Hello World!')
})

mongoose.connect(process.env.MONGODB, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('Conexión a MongoDB establecida'))
  .catch(err => console.log(err))

app.use('/api/v1', require('./src/routers/routers'));

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})